import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: function () {
      return import('../views/Home.vue')
    }
  },
  {
    path: '/ip-addresses',
    name: 'IP Address',
    meta: {
      requiresAuth: true
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/IPAddress.vue')
    }
  },
  {
    path: '/login',
    name: 'login',
    component: function () {
      return import ('../views/Login.vue')
    },
    props: true
  },
  {
    path: '/ip-addresses/create',
    name: 'New IP Address',
    meta: {
      requiresAuth: true
    },
    component: function () {
      return import('../views/AddIPAddress.vue')
    }
  },
  {
    path: '/ip-addresses/:id/edit',
    name: 'edit-ip-address',
    meta: {
      requiresAuth: true
    },
    component: function () {
      return import('../views/EditIPAddress.vue')
    },
    props: true
  },
  {
    path: '/audit-trail',
    name: 'audit-trail',
    meta: {
      requiresAuth: true
    },
    component: function () {
      return import('../views/AuditTrail.vue')
    }
  }
]

let router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (localStorage.getItem('token') == null) {
      next({
        path: '/login',
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
