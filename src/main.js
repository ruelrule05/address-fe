import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from 'axios';
import VueAxios from 'vue-axios';
import './assets/tailwind.css'

Vue.use(VueAxios, axios)

Vue.axios.defaults.withCredentials = true
Vue.axios.defaults.baseURL = 'http://127.0.0.1:8000/';

Vue.config.productionTip = false

Vue.axios.interceptors.request.use(function (config) {
  const token = localStorage.getItem('token');

  if (token != null) {
    config.headers.Authorization = 'Bearer ' + token
  }

  return config;
})

Vue.axios.interceptors.response.use(function (response) {
  return response;
}, function(err) {
  if (err.response) {
    if (err.response.status === 401) {
      localStorage.removeItem('token');

      alert('Your session has expired, please login again.');

      router.push({ path: '/login' })
    }
  }

  return Promise.reject(err)
})

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
