# front-end

## Project setup
```
npm install
```

Before running the command below, make sure to update the main.js file on line 13 if the backend is running on a different IP address and port

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
